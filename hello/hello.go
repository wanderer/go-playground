package main

import (
	"fmt"

	"example/user.hello/moarstrings"

	"github.com/google/go-cmp/cmp"
)

func main() {
	fmt.Println(moarstrings.ReverseRunes("!oG ,olleH"))
	fmt.Println(cmp.Diff("Hello World", "Hello Go"))
}
