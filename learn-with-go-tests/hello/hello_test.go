package main

import "testing"

func TestHello(t *testing.T) {
	assertCorrectMessage := func(t testing.TB, got, want string) {
		t.Helper() // tells the test suite that this method is a helper

		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("saying hello to people", func(t *testing.T) {
		got := Hello("Dude", "")
		want := englishHelloPrefix + "Dude"
		assertCorrectMessage(t, got, want)
	})
	t.Run("say 'Hello, World' on empty string", func(t *testing.T) {
		got := Hello("", "")
		want := englishHelloPrefix + "World"
		assertCorrectMessage(t, got, want)
	})
	t.Run("say hello in Slovak", func(t *testing.T) {
		got := Hello("Alfonz", "Slovak")
		want := slovakHelloPrefix + "Alfonz"
		assertCorrectMessage(t, got, want)
	})
	t.Run("say hello in French", func(t *testing.T) {
		got := Hello("Emmanuel", "French")
		want := "Bonjour, " + "Emmanuel"
		assertCorrectMessage(t, got, want)
	})
}
