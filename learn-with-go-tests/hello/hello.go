package main

import "fmt"

const (
	slovak             = "Slovak"
	french             = "French"
	englishHelloPrefix = "Hello, "
	slovakHelloPrefix  = "Ahoj, "
	frenchHelloPrefix  = "Bonjour, "
)

// Hello prints "Hello, World" if nothing is passed to it, otherwise it prints
// "Hello, "+ whatever string is passed to it
func Hello(name string, language string) string {
	if name == "" {
		name = "World"
	}

	return greetingPrefix(language) + name
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case slovak:
		prefix = slovakHelloPrefix
	case french:
		prefix = frenchHelloPrefix
	default:
		prefix = englishHelloPrefix
	}

	return
}

func main() {
	fmt.Println(Hello("", ""))
	fmt.Println(Hello("Dude", ""))
}
