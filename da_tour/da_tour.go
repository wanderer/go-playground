// following the Tour of GO tutorial
package main

import "example/user.da_tour/datour"

func main() {
	datour.MultipleResults()
	datour.Variables()
	datour.VariablesWithInitializers()
	datour.ShortVariableDeclarations()
	datour.For()
	// exercise to write sqrt
	datour.DoSqrt()
	datour.Switch()
	datour.SwitchEvalOrder()
	datour.SwitchWithNoCondition()
	datour.Defer()
	datour.DeferMulti()
}
