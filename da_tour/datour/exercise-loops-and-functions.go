package datour

import (
	"fmt"
)

func sqrt(x float64) float64 {
	// multiple initial values for z to manually check the convergence rate
	// z := x
	// z := 1.0
	z := x / 4
	fmt.Println("\nSqrt of:", x, "; 10-fold approx.")
	fmt.Println()
	for i := 0; i < 10; i++ {
		fmt.Println(z)
		z -= (z*z - x) / (2 * z)
	}
	return z
}

// DoSqrt method performs a custom sqrt function
func DoSqrt() {
	for i := 2; i < 26; i++ {
		sqrt(float64(i))
	}
}
