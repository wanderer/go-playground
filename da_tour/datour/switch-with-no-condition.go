package datour

import (
	"fmt"
	"time"
)

func SwitchWithNoCondition() {
	// Switch without a condition is the same as switch true.
	// This construct can be a clean way to write long if-then-else chains.

	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Godmorgen!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Godaften.")
	}
}
