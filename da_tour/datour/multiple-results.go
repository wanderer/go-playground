package datour

import "fmt"

func swap(x, y string) (string, string) {
	return y, x
}

// MultipleResults func utilizes function swap that returns multiple results
func MultipleResults() {
	a, b := swap("hello!", "Go world,")
	fmt.Println(a, b)
}
