package datour

import "fmt"

var i, j = 1, 2

func VariablesWithInitializers() {
	var c, python, java = true, false, "no!"
	fmt.Println(i, j, c, python, java)
}
