package datour

import "fmt"

func Pointers() {
	i, j := 24, 2701

	p := &i         // point to i
	fmt.Println(*p) // read i through the pointer
	*p = 21

	p = &j
}
