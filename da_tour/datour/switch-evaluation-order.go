package datour

import (
	"fmt"
	"time"
)

func SwitchEvalOrder() {
	fmt.Println("When's Saturday?")
	today := time.Now().Weekday()
	switch time.Saturday {
	case today + 0:
		fmt.Println("\t> Today.")
	case today + 1:
		fmt.Println("\t> Tomorrow.")
	case today + 2:
		fmt.Println("\t> In two days.")
	default:
		fmt.Println("\t> Too far away.")
	}
}
